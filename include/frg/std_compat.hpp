#pragma once
#include <cstddef>

namespace frg {

struct stl_allocator {
#   ifndef __clang__
	void *allocate(size_t size) {
		return operator new(size);
	}

	void deallocate(void *ptr, size_t size) {
		operator delete(ptr, size);
	}

	void free(void *ptr) {
		operator delete(ptr);
    }
#   else
    void *allocate(size_t size) {
        return operator new(size);
    }

    void deallocate(void *ptr, size_t size) {
        operator delete(ptr);
    }

    void free(void *ptr) {
        operator delete(ptr);
    }
#   endif
};

} // namespace frg
